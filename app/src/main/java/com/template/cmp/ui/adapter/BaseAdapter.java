package com.template.cmp.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.template.cmp.ui.adapter.holder.BaseViewHolder;

import java.util.List;

public abstract class BaseAdapter<T, VH extends BaseViewHolder<T>> extends RecyclerView.Adapter<VH> {

    List<T> items;

    BaseAdapter(List<T> items) {
        this.items = items;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.bindData(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateItems(List<T> newItems) {
        items = newItems;
        notifyDataSetChanged();
    }
}
