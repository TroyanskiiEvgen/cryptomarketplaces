package com.template.cmp.ui.views;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.template.cmp.ui.model.TabData;

import butterknife.ButterKnife;

/**
 * Created by Relax on 06.06.2017.
 */

public abstract class BaseTab extends LinearLayout {

    private TabData mTabData;
    private boolean isSelected;

    public BaseTab(Context context) {
        super(context);
    }

    public BaseTab(Context context, TabData tabData) {
        super(context);
        mTabData = tabData;
        setContentView();
        setupViews(tabData, isSelected);
    }


    private void setContentView() {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(getLayoutRes(), this, true);
        ButterKnife.bind(this);
    }


    @LayoutRes
    protected abstract int getLayoutRes();


    protected abstract void setupViews(TabData tabData, boolean isSelected);


    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
        setupViews(mTabData, isSelected);
    }


    @Override
    public boolean isSelected() {
        return isSelected;
    }
}
