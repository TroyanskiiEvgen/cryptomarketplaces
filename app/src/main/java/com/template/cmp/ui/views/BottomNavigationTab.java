package com.template.cmp.ui.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.template.cmp.ui.model.TabData;
import com.template.cmp.R;

import butterknife.BindView;

/**
 * Created by Relax on 06.06.2017.
 */

@SuppressLint("ViewConstructor")
public class BottomNavigationTab extends BaseTab {

    @BindView(R.id.tab_icon) ImageView image;
    @BindView(R.id.tab_title) TextView title;

    public BottomNavigationTab(Context context, TabData tabData) {
        super(context, tabData);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.view_bottom_nav_tab;
    }


    @Override
    protected void setupViews(TabData tabData, boolean isSelected) {
        image.setImageResource(isSelected ? tabData.getImageSelected() : tabData.getImageDefault());
        title.setText(tabData.getTitle());
        title.setTextColor(ContextCompat.getColor(getContext(),
                isSelected ? tabData.getSelectedColor() : tabData.getDefaultColor()));
    }
}
