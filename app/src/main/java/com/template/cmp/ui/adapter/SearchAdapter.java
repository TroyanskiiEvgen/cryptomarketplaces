package com.template.cmp.ui.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.template.cmp.R;
import com.template.cmp.model.BuisenessPlace;
import com.template.cmp.ui.adapter.holder.SearchHolder;

import java.util.List;

public class SearchAdapter extends BaseAdapter<BuisenessPlace, SearchHolder> {

    public SearchAdapter(List<BuisenessPlace> items) {
        super(items);
    }

    @NonNull
    @Override
    public SearchHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new SearchHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_search_place, viewGroup, false));
    }
}
