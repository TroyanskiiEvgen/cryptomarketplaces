package com.template.cmp.ui.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Relax on 06.06.2017.
 */

public class TabData implements Parcelable {

    private int imageDefault;
    private int imageSelected;
    private int title;
    private int defaultColor;
    private int selectedColor;


    public TabData(int imageDefault, int imageSelected, int title, int defaultTextColor, int selectedTextColor) {
        this.imageDefault = imageDefault;
        this.imageSelected = imageSelected;
        this.title = title;
        this.defaultColor = defaultTextColor;
        this.selectedColor = selectedTextColor;
    }

    public int getImageDefault() {
        return imageDefault;
    }

    public void setImageDefault(int imageDefault) {
        this.imageDefault = imageDefault;
    }

    public int getImageSelected() {
        return imageSelected;
    }

    public void setImageSelected(int imageSelected) {
        this.imageSelected = imageSelected;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getDefaultColor() {
        return defaultColor;
    }

    public void setDefaultColor(int defaultColor) {
        this.defaultColor = defaultColor;
    }

    public int getSelectedColor() {
        return selectedColor;
    }

    public void setSelectedColor(int selectedColor) {
        this.selectedColor = selectedColor;
    }

    protected TabData(Parcel in) {
        imageDefault = in.readInt();
        imageSelected = in.readInt();
        title = in.readInt();
        defaultColor = in.readInt();
        selectedColor = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(imageDefault);
        dest.writeInt(imageSelected);
        dest.writeInt(title);
        dest.writeInt(defaultColor);
        dest.writeInt(selectedColor);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TabData> CREATOR = new Creator<TabData>() {
        @Override
        public TabData createFromParcel(Parcel in) {
            return new TabData(in);
        }

        @Override
        public TabData[] newArray(int size) {
            return new TabData[size];
        }
    };
}
