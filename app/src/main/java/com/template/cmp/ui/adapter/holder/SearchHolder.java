package com.template.cmp.ui.adapter.holder;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.template.cmp.R;
import com.template.cmp.model.BuisenessPlace;

import butterknife.BindView;

public class SearchHolder extends BaseViewHolder<BuisenessPlace> {

    @BindView(R.id.item_search_image) ImageView itemImage;
    @BindView(R.id.item_search_title) TextView itemTitle;
    @BindView(R.id.item_search_description) TextView itemDescription;

    public SearchHolder(@NonNull View itemView) {
        super(itemView);
    }

    @Override
    public void bindData(BuisenessPlace item) {
        Glide.with(itemImage.getContext())
                .load(item.getLogoImage())
                .into(itemImage);
        itemTitle.setText(item.getName());
        itemDescription.setText(item.getDescription());
    }
}
