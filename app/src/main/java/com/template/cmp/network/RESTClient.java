package com.template.cmp.network;

public class RESTClient {


    private final static String TAG = RESTClient.class.getSimpleName();

    private static RESTClient instance;

    private RESTClient() {
    }

    public static synchronized RESTClient getInstance() {
        if (instance == null) {
            synchronized (RESTClient.class) {
                if (instance == null) {
                    instance = new RESTClient();
                }
            }
        }
        return instance;
    }


}
